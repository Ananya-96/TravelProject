
var firstSeatLabel = 1;

$(document).ready(function() {
  var $cart = $('#selected-seats'),
    $counter = $('#counter'),
    $total = $('#total'),
    sc = $('#seat-map').seatCharts({
    map: [
      'ee_e',
      'ee_e',
      'ee_e',
      'ee_e',
      'ee_e',

    ],
    seats: {

      e: {
        price   : 40,
        classes : 'economy-class',
        category: 'Economy Class'
      }

    },
    naming : {
      top : false,
      getLabel : function (character, row, column) {
        return firstSeatLabel++;
      },
    },
    legend : {
      node : $('#legend'),
      items : [

        [ 'e', 'available',   'Available'],
        [ 'f', 'unavailable', 'Already Booked']

      ]

    },
    click: function () {
      if (this.status() == 'available') {

        $('<li>'+this.data().category+' : Seat no '+this.settings.label+': <b>₹'+this.data().price+'</b> <a href='#' class='cancel-cart-item'>[cancel]</a></li>')
          .attr('id', 'cart-item-'+this.settings.id)
          .data('seatId', this.settings.id)
          .appendTo($cart);


        $counter.text(sc.find('selected').length+1);
        $total.text(recalculateTotal(sc)+this.data().price);

        return 'selected';
      } else if (this.status() == 'selected') {

        $counter.text(sc.find('selected').length-1);

        $total.text(recalculateTotal(sc)-this.data().price);


        $('#cart-item-'+this.settings.id).remove();


        return 'available';
      } else if (this.status() == 'unavailable') {

        return 'unavailable';
      } else {
        return this.style();
      }
    }
  });


  $('#selected-seats').on('click', '.cancel-cart-item', function () {
      sc.get($(this).parents('li:first').data('seatId')).click();
  });


sc.get[js_array].status('unavailable');

});

function recalculateTotal(sc) {
var total = 0;


sc.find('selected').each(function () {
  total += this.data().price;
});

return total;
}
