<html><!--<![endif]-->
<head>
	<meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="">
    <meta name="author" content="">

	<title>Booking</title>

	<!-- Standard Favicon -->
	<link rel="icon" type="image/x-icon" href="images/favicon.png" />
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">

	<!-- For iPhone 4 Retina display: -->
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="images//apple-touch-icon-114x114-precomposed.png">

	<!-- For iPad: -->
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="images//apple-touch-icon-72x72-precomposed.png">

	<!-- For iPhone: -->
	<link rel="apple-touch-icon-precomposed" href="images//apple-touch-icon-57x57-precomposed.png">

	<!-- Library - Google Font Familys -->

	<!-- Library - Bootstrap v3.3.5 -->
    <link rel="stylesheet" type="text/css" href="libraries/lib.css">
    <link rel="stylesheet" type="text/css" href="libraries/calender/calendar.css">

	<!-- Custom - Common CSS -->
	<link rel="stylesheet" type="text/css" href="css/plugins.css">
	<link rel="stylesheet" type="text/css" href="css/navigation-menu.css">

	<!-- Custom - Theme CSS -->
	<link rel="stylesheet" type="text/css" href="style.css" />
	<link rel="stylesheet" type="text/css" href="css/shortcodes.css" />

	<!--[if lt IE 9]>
		<script src="js/html5/respond.min.js"></script>
    <![endif]-->

<script type="text/javascript" src="https://gc.kis.v2.scr.kaspersky-labs.com/6A4144BC-684E-BB42-ABA5-B5A0264410C3/main.js" charset="UTF-8"></script><link rel="stylesheet" crossorigin="anonymous" href="https://gc.kis.v2.scr.kaspersky-labs.com/3C0144620A5B-5ABA-24BB-E486-CB4414A6/abn/main.css"/></head>

<body data-offset="200" data-spy="scroll" data-target=".ow-navigation">
	<!-- Loader -->
	<div id="site-loader" class="load-complete">
		<div class="loader">
			<div class="loader-inner ball-clip-rotate">
				<div></div>
			</div>
		</div>
	</div><!-- Loader /- -->

	<a id="top"></a>

	<!-- Header Section -->
	<header id="header" class="header-section header-position container-fluid no-padding">
		<!-- Top Header -->
		<div class="top-header container-fluid no-padding">
			<!-- Container -->
			<div class="container">
				<div class="row">
					<div class="logo-block col-md-3"><a href="index.html" title="RPM"><img src="images/logo.png" alt="Logo" /></a></div>
					<div class="col-md-9 contact-detail">
						<div class="phone">
							<img src="images/phone-ic.png" alt="Phone" />
							<h6>contact us</h6>
							<a href="tell:7200037090" title="7200037090">7200037090</a>
						</div>
						<div class="address">

							<h6>Email</h6>
							<p>info@rpmtravels.com</p>
						</div>

					</div>
				</div>
			</div><!-- Container /- -->
		</div><!-- Top Header /- -->
		
	</header><!-- Header Section /- -->

	<main class="site-main page-spacing">
		
                 <div class="section-padding"></div>
<div class="section-padding"></div>
		
		<!-- Container -->
		<div class="container">
			<div class="row">
                             
				<!-- Contenta Area -->
				<div class="col-md-8 col-sm-8 col-xs-12 content-area">
                                <h2> Close Trip </h2>
					<div class="form-group col-md-12 col-sm-12 col-xs-12">
					<a href="close1.php"><input class="read-more" type="submit" value="Close Trip ch - ban 1"></a>
					</div>
                                       <div class="form-group col-md-12 col-sm-12 col-xs-12">
					<a href="close2.php"><input class="read-more" type="submit" value="Close Trip ch - ban 2"></a>
					</div> 
                                         <div class="form-group col-md-12 col-sm-12 col-xs-12">
					<a href="close3.php"><input class="read-more" type="submit" value="Close Trip ban - ch 1"></a>
					</div>
                                         <div class="form-group col-md-12 col-sm-12 col-xs-12">
					<a href="close4.php"><input class="read-more" type="submit" value="Close Trip ban - ch 2"></a>
					</div>

                              <h2> Close Package Trip </h2>
					<div class="form-group col-md-12 col-sm-12 col-xs-12">
					<a href="closep1.php"><input class="read-more" type="submit" value="Close Trip 1"></a>
					</div>
                                       <div class="form-group col-md-12 col-sm-12 col-xs-12">
					<a href="closep2.php"><input class="read-more" type="submit" value="Close Trip 2"></a>
					</div> 
                                         <div class="form-group col-md-12 col-sm-12 col-xs-12">
					<a href="closep3.php"><input class="read-more" type="submit" value="Close Trip 3"></a>
					</div>
                                         <div class="form-group col-md-12 col-sm-12 col-xs-12">
					<a href="closep4.php"><input class="read-more" type="submit" value="Close Trip 4"></a>
					</div>
                                         <div class="form-group col-md-12 col-sm-12 col-xs-12">
					<a href="closep4.php"><input class="read-more" type="submit" value="Close Trip 5"></a>
					</div>

                           
                          <h2> Book Ticket </h2>
                                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
					<a href="bookprev2.php"><input class="read-more" type="submit" value="Book Tickets Ch- Bang"></a>
					</div>
                                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
					<a href="bookprev3.php"><input class="read-more" type="submit" value="Book Tickets Bang - Ch"></a>
					</div>
                                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
					<a href="book-package.php"><input class="read-more" type="submit" value="Book Tickets Package"></a>
					</div>
                             
                         <h2> Cancel Ticket </h2>           
                                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
					<a href="cancel1.php"><input class="read-more" type="submit" value="Cancel Tickets Ch- Bang 1"></a>
					</div>
                                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
					<a href="cancel2.php"><input class="read-more" type="submit" value="Cancel Tickets Ch- Bang 2"></a>
					</div>
                                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
					<a href="cancel3.php"><input class="read-more" type="submit" value="Cancel Tickets Bang- Ch 1"></a>
					</div>
                                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
					<a href="cancel4.php"><input class="read-more" type="submit" value="Cancel Tickets Bang - Ch 2"></a>
					</div>
                              
                         <h2> View Trip Details </h2>
                                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
					<a href="view1.php"><input class="read-more" type="submit" value="View Trip ch- bang 1">
					</div>
                                       <div class="form-group col-md-12 col-sm-12 col-xs-12">
					<a href="view2.php"><input class="read-more" type="submit" value="View Trip ch - bang 2">
					</div> 
                                         <div class="form-group col-md-12 col-sm-12 col-xs-12">
					<a href="view3.php"><input class="read-more" type="submit" value="View Trip bang - ch 1">
					</div>
                                         <div class="form-group col-md-12 col-sm-12 col-xs-12">
					<a href="view4.php"><input class="read-more" type="submit" value="View Trip bang- ch 2">
					</div>
                                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
					<a href="view5.php"><input class="read-more" type="submit" value=" View Package">
					</div>

                       <h2> Change Price </h2>


                                       <div class="form-group col-md-12 col-sm-12 col-xs-12">
					<a href="change1.php"><input class="read-more" type="submit" value="Change Price Ch- Bang 1"></a>
					</div>
                                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
					<a href="change2.php"><input class="read-more" type="submit" value="Change Price Ch- Bang 2"></a>
					</div>
                                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
					<a href="change3.php"><input class="read-more" type="submit" value="Change Price Bang- Ch 1"></a>
					</div>
                                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
					<a href="change4.php"><input class="read-more" type="submit" value="Change Price Bang - Ch 2"></a>
					</div>
                              
                                        
                                </div>
                                <!-- Contenta Area /- -->
				
			</div>
		</div><!-- Container /- -->

		<div class="section-padding"></div>
	</main>

	<!-- Footer Section -->
	<footer class="w3-center w3-padding-64" style="background-color: #333333">

  <p style="color: white">Powered by<br>
<img src="images/foot.jpg" /></p>
</footer>
	<!-- JQuery v1.11.3 -->
	<script src="js/jquery.min.js"></script>

	<!-- Library JS -->
	<script src="libraries/lib.js"></script>
	<script src="libraries/calender/jquery-ui-datepicker.min.js"></script>
	<script src="http://code.jquery.com/jquery-migrate-1.0.0.js"></script>
	<!-- Library - Google Map API -->
	<script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>


	<!-- Library - Theme JS -->
	<script src="js/functions.js"></script>

</body>
</html>
